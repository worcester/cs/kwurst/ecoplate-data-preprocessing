# -*- coding: utf-8 -*-
"""
Ecoplate Preprocessing

Karl R. Wurst

Copyright 2023

This program is free software: you can redistribute it and/or modify it under
the terms of the GNU General Public License as published by the Free Software
Foundation, either version 3 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with 
this program. If not, see <https://www.gnu.org/licenses/>. 
"""

import pandas
import statistics
import os
import re

def main():
    filelist = get_all_ecoplate_files("datafiles")
    samplerows = process_ecoplate_files(filelist)
    samplerows.to_csv("datafiles/samples.csv")

def process_ecoplate_files(filelist):
    newsamplerows = new_empty_sample_rows()

    row = 1
    for file in filelist:
        datablock = read_ecoplate_file(file)
        datarows =  convert_blocks_to_rows(datablock)
        samplerow = process_triplicate_rows(datarows)
                
        newsamplerows.loc[row] = year_site_sample(file) + samplerow
        row = row + 1
    
    return newsamplerows

def read_ecoplate_file(filepath):
    datablock = pandas.read_table(filepath, skiprows=7, header="infer", index_col=0)
    return datablock

def convert_blocks_to_rows(datablock):
    datarows = new_empty_rows()
    blocks = [ \
              ["1","2","3","4"], \
              ["5","6","7","8"], \
              ["9","10","11","12"] \
             ]

    rownum = 1
    for block in blocks:    
        
        newrow = []
        for col in block:
            for row in datablock.index:
                if not starts_with_exclamation(datablock[col][row]):
                    newrow.append(float(datablock[col][row]))
                else:
                    newrow.append(datablock[col][row])                 

        datarows.loc[rownum] = subtract_control_from_samples(newrow)
        rownum = rownum + 1
    
    return datarows
 
def process_triplicate_rows(datarows):
    newdatarow = []

    for col in datarows:
        newdatarow.append(process_triplicate_col(datarows, col))

    return newdatarow
 
def process_triplicate_col(datarows, col):
    datacol = get_col(datarows, col)
    datacol = drop_exception_values(datacol)
    datacol = drop_values_outside_range(datacol)
    return average(datacol)
    
def get_col(datarows, col):
    return datarows[col].tolist()
    
def drop_exception_values(datacol):
    return [value for value in datacol if not starts_with_exclamation(value)]
    
def drop_values_outside_range(datacol):
    if min(datacol) < max(datacol) / 2:
        datacol.remove(min(datacol))
    return datacol

def average(datacol):
    if len(datacol) == 0:
        return 0
    else:
        return statistics.mean(datacol)
    
def year_site_sample(file):
    return re.split('_|\.|/', file)[1:4]

def subtract_control_from_samples(datarow):
    for col in range(1, len(datarow)):
        if not starts_with_exclamation(datarow[col]):
            datarow[col] = datarow[col] - datarow[0]
            if datarow[col] < 0:
                datarow[col] = 0
    return datarow
   
def starts_with_exclamation(value):
    return isinstance(value, str) and value.startswith("!")
    
def get_all_ecoplate_files(directory):
    files = [directory + "/" + f \
                 for f in os.listdir(directory) \
                     if os.path.isfile(directory+'/'+f) \
                            and f.endswith(".TXT") \
            ]
    return files

def new_empty_sample_rows():
    return pandas.DataFrame( \
                columns=["Year", "Site", "Sample", "M1","M2","M3","M4","M5","M6","M7","M8", \
                        "M9","M10","M11","M12","M13","M14","M15","M16", \
                        "M17","M18","M19","M20","M21","M22","M23","M24", \
                        "M25","M26","M27","M28","M29","M30","M31","M32",])
        
def new_empty_rows():
    return pandas.DataFrame( \
                columns=["M1","M2","M3","M4","M5","M6","M7","M8", \
                        "M9","M10","M11","M12","M13","M14","M15","M16", \
                        "M17","M18","M19","M20","M21","M22","M23","M24", \
                        "M25","M26","M27","M28","M29","M30","M31","M32",])
    
if __name__ == "__main__":
    main()